﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumWithCSharp.NET.Base;
using SeleniumWithCSharp.NET.Extensions;
using SeleniumWithCSharp.NET.PageObject;
using SeleniumWithCSharp.NET.PageObject.Login;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SeleniumWithCSharp.NET.Base.Browser;

namespace SeleniumWithCSharp.NET
{
    public class TestScriptBase
    {
        protected string testName;
        protected IWebDriver Driver;
        protected BasePage basePage;
        protected Browser browser;
        protected HomePageHelper homePageHelper;
        protected HomePageElement homePageElement;
        protected LogInHelper logInHelper;
        protected LogInElement logInElement;
        protected URLNav uRLNav;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Driver = DriverFactory.GetInstance("Chrome").GetDriver();
            Driver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            //uRLNav = new URLNav();
            //Driver.Navigate().GoToUrl(uRLNav.GoToAutomationPractice(ScreenType.Laptop,""));
            //uRLNav.GoToAutomationPractice(ScreenType.Laptop, "");
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(30);
            
            basePage = new BasePage();
            browser = new Browser();
            
            homePageHelper = new HomePageHelper();
            homePageElement = new HomePageElement();
            logInElement = new LogInElement();
            logInHelper = new LogInHelper();

            //browser.OpenScreen(ScreenType.Laptop);
            Driver.Manage().Window.Maximize();
        }

        public void OneTimeDown()
        {
            browser = null;
            DriverFactory.GetInstance().RemoveDriver();
        }

        public static string GetCurrentClassName()
        {
            var stackTrace = new StackTrace(); // get call stack
            var stackFrames = stackTrace.GetFrames(); // get method calls (frames)
            foreach (var stackFrame in stackFrames)
            {
                if ((stackFrame.GetMethod().ReflectedType.BaseType == typeof(BasePage) || stackFrame.GetMethod().ReflectedType.BaseType == typeof(WebDriverExtension)) &&
                    (!stackFrame.GetMethod().IsConstructor))
                {
                    return stackFrame.GetMethod().ReflectedType.Name;
                }
            }

            foreach (var stackFrame in stackFrames)
            {
                if ((stackFrame.GetMethod().ReflectedType.BaseType == typeof(TestScriptBase)) && (!stackFrame.GetMethod().IsConstructor))
                {
                    return stackFrame.GetMethod().ReflectedType.Name + "." + stackFrame.GetMethod().Name;
                }
            }

            return string.Empty;
        }
    }
}
