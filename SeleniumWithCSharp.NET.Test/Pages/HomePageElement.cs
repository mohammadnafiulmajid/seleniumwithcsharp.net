﻿using OpenQA.Selenium;
using SeleniumWithCSharp.NET.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.Test.Pages
{
   public class HomePageElement : BasePage
    {
        public IWebElement Logo => waitUntilElementExists(By.XPath("//img[@class='logo img-responsive']"));
        public IWebElement ContactUsText => waitUntilElementExists(By.XPath("//div[@id='contact-link']//a[contains(text(),'Contact us')]"));
        
    }
}
