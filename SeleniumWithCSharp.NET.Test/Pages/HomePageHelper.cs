﻿using SeleniumWithCSharp.NET.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.PageObject
{
    public class HomePageHelper : BasePage
    {
        HomePageElement homePageElement = new HomePageElement();

        public string ContactUsTextMessage()
        {
            string contavtUs = homePageElement.ContactUsText.Text;
            return contavtUs;
        }
    }
}
