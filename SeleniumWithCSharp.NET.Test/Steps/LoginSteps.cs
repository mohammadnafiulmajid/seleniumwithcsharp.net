﻿using NUnit.Framework;
using SeleniumWithCSharp.NET.PageObject.Login;
using TechTalk.SpecFlow;

namespace SeleniumWithCSharp.NET.Test.Steps
{
 
    [Parallelizable]
    [Binding]
    public class LoginSteps : TestScriptBase
    {
        [When(@"Browse to the url ""(.*)""")]
        public void WhenBrowseToTheUrl(string p0)
        {
            OneTimeSetUp();
            uRLNav = new Extensions.URLNav();            
            uRLNav.GoToAutomationPractice(Base.Browser.ScreenType.BigScreen);
        }

        [Then(@"automationpractice home page should show")]
        public void ThenAutomationpracticeHomePageShouldShow()
        {
            homePageHelper.VerifyPageTitle("My Store");
            Assert.IsTrue(homePageHelper.ContactUsTextMessage().Contains("Contact us"), "Contact us text was not present");
        }

        [When(@"User click signin button")]
        public void WhenUserClickSigninButton()
        {
            homePageHelper.NavigateToLoginPage();
        }

        [When(@"User entered username into the username field")]
        public void WhenUserEnteredUsernameIntoTheUsernameField()
        {
            logInHelper.EnterUserValidEmail(UsersCreds.User1);
        }

        [When(@"User entered password into the password field")]
        public void WhenUserEnteredPasswordIntoThePasswordField()
        {
            logInHelper.EnterUserValidPassword();
        }

        [When(@"User press signin button")]
        public void WhenUserPressLoginButton()
        {
            logInHelper.ClickSigninBtn();
        }

        [Then(@"User should see the username")]
        public void ThenUserShouldSeeTheUsername()
        {
            Assert.IsTrue(logInHelper.VerifyLoginUserName().Equals("LG TEST"));
        }


    }
}
