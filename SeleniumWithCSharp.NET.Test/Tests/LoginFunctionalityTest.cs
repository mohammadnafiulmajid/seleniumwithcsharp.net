﻿using NUnit.Framework;
using SeleniumWithCSharp.NET.Extensions;
using SeleniumWithCSharp.NET.PageObject.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.Test.Tests
{
   [TestFixture]
   [Parallelizable]
   public class LoginFunctionalityTest : TestScriptBase
    {
        [SetUp]
        public void Setup()
        {
            uRLNav = new Extensions.URLNav();           
        }

        [Test,Category("smoke")]
        public void LoginWithValidCredentials()
        {            
            uRLNav.GoToAutomationPractice(Base.Browser.ScreenType.BigScreen);
            homePageHelper.VerifyPageTitle("My Store");
            Assert.IsTrue(homePageHelper.ContactUsTextMessage().Contains("Contact us"),"Contact us text was not present");
            homePageHelper.NavigateToLoginPage();
            logInHelper.VerifyPageTitle("Login - My Store");
            logInHelper.LogInAsUser(UsersCreds.User1);
            logInHelper.ClickSigninBtn();
            Assert.IsTrue(logInHelper.VerifyLoginUserName().Equals("LG TEST"));
        }
    }
}
