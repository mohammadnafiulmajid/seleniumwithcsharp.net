﻿using OpenQA.Selenium;
using SeleniumWithCSharp.NET.Base;
using SeleniumWithCSharp.NET.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.PageObject
{
    public class HomePageElement : AutomationBaseHelpers
    {
        public IWebElement Logo => WaitUntilElementExists(By.XPath("//img[@class='logo img-responsive']"));
        public IWebElement ContactUsText => WaitUntilElementExists(By.XPath("//div[@id='contact-link']//a[contains(text(),'Contact us')]"));
        public IWebElement SignBtn => WaitUntilElementExists(By.XPath("//*[@class='login']"));
        public IWebElement SearchBox => WaitUntilElementExists(By.Id("searchbox"));
        public IWebElement SearchBtn => WaitUntilElementExists(By.XPath("//*[@name='submit_search']"));
    }
}
