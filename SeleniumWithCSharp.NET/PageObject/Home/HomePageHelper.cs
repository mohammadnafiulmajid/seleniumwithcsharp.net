﻿using SeleniumWithCSharp.NET.Base;
using SeleniumWithCSharp.NET.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.PageObject
{
    public class HomePageHelper : AutomationBaseHelpers
    {
        HomePageElement homePageElement = new HomePageElement();

        public void NavigateToLoginPage()
        {
            homePageElement.SignBtn.Click();            
        }

        public string ContactUsTextMessage()
        {
            string contavtUs = homePageElement.ContactUsText.Text;
            return contavtUs;
        }
    }
}
