﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.PageObject.Login
{
    public class JsonUserInfo
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string Id { get; set; }
    }

    public class LoginUser
    {
        public List<JsonUserInfo> Data { get; set; }
    }
}
