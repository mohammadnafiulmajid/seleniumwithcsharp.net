﻿using SeleniumWithCSharp.NET.Extensions;
using SeleniumWithCSharp.NET.Utilities;
using SeleniumWithCSharp.NET.Utilities.ResourceFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.PageObject.Login
{
    public class LogInHelper : AutomationBaseHelpers
    {
        private LogInElement logInElement = new LogInElement();

        public void LogInAsUser(UsersCreds user)
        {
            LoginUser userData = jsonUserInfoHelpers.GetUserFromJsonFile();
            JsonUserInfo testUser1 = userData.Data.Find(i => i.Id == Constants.UserId1);

            switch (user)
            {
                case UsersCreds.User1:
                    {
                        LapsedTimeIfIEBrowser(2000);
                        TypeText(logInElement.EmailField, testUser1.Username);
                        TypeText(logInElement.PasswordField, testUser1.Password);
                        WaitElapsedTime(100);
                        break;
                    }
            }
        }

        public void EnterUserValidEmail(UsersCreds user)
        {
            LoginUser userData = jsonUserInfoHelpers.GetUserFromJsonFile();
            JsonUserInfo testUser1 = userData.Data.Find(i => i.Id == Constants.UserId1);
            WaitElapsedTime(100);
            TypeText(logInElement.EmailField, testUser1.Username);
        }

        public void EnterUserValidPassword()
        {
            WaitElapsedTime(100);
            TypeText(logInElement.PasswordField, UserCredentials.password);
        }

        public void ClickSigninBtn()
        {
            WaitElapsedTime(100);
            Click(logInElement.SigninBtn);
        }

        public string VerifyLoginUserName()
        {
            string UserName = logInElement.UserName.Text;
            return UserName;
        }
    }
}

