﻿using OpenQA.Selenium;
using SeleniumWithCSharp.NET.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.PageObject.Login
{
    public enum UsersCreds
    {
        User1
    }

    public class LogInElement : AutomationBaseHelpers
    {
        public IWebElement EmailField => WaitUntilElementExists(By.Id("email"));
        public IWebElement PasswordField => WaitUntilElementExists(By.Id("passwd"));
        public IWebElement SigninBtn => WaitUntilElementExists(By.XPath("//p[@class='submit']//span[1]"));
        public IWebElement UserName => WaitUntilElementExists(By.ClassName("account"));
        
    }
}
