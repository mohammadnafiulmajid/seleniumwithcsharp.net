﻿using Newtonsoft.Json;
using SeleniumWithCSharp.NET.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.PageObject.Login
{
   public class JsonUserInfoHelpers
    {
        private Config config;

        public LoginUser GetUserFromJsonFile()
        {
            config = new Config();
            //string path = AppDomain.CurrentDomain.BaseDirectory;
            //string data = File.ReadAllText(path + "/../.." + config.GetUserCredentialsPath());
            string data = File.ReadAllText(@"C:\MyDevelopments\SeleniumWithCSharp.NET\SeleniumWithCSharp.NET\Utilities\TestDataJSonFormat\UserCredentials\UserCredentials.json");
            //string data = File.ReadAllText(@"C:\MyDevelopments\SeleniumWithCSharp.NET\SeleniumWithCSharp.NET\Utilities\TestDataJSonFormat\UserCredentials\UserCredentials.json");
            LoginUser userInfo = JsonConvert.DeserializeObject<LoginUser>(data);
            return userInfo;
        }
    }
}
