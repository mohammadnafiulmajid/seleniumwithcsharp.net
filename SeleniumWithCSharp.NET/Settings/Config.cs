﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.Settings
{
    using System;
    using System.Configuration;

    public class  Config : IConfig
    {
        public static object Settings { get; internal set; }

        public string GetBaseUrl()
        {
            return ConfigurationManager.AppSettings["AutomationUrl"];
        }

        public string GetPageErrorDataPath()
        {
            return ConfigurationManager.AppSettings["PageErrorJsonFilePath"];
        }
        public string GetUserCredentialsPath()
        {
            return ConfigurationManager.AppSettings["UserCredentialsPath"];
        }

        
    }
}
