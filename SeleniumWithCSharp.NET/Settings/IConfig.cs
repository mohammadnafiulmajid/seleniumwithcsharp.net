﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.Settings
{
    public interface IConfig
    {
        string GetBaseUrl();
        string GetPageErrorDataPath();
    }
}
