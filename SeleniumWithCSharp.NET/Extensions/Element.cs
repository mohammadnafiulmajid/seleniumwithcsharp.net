﻿using OpenQA.Selenium.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;
using System.Collections;
using System.Drawing;
using SeleniumWithCSharp.NET.Base;
using System.Threading;
using SeleniumWithCSharp.NET.Settings;

namespace SeleniumWithCSharp.NET.Extensions
{
    public class Element : IWrapsDriver, IWrapsElement, IEnumerable<Element>
    {
        private By by;
        private string name;
        private string pageObjectName = string.Empty;
        private int timeoutSec;
        private Element root;
        private Frame frame1;
        private IWebElement element;
        private IEnumerable<Element> elements1;
        //private ElementImages images1;
        //private StackFrame[] stackFrames;

        IWebDriver Driver = DriverFactory.GetInstance().GetDriver();

        public Element()
        {
            this.GetName();
            //this.PageObjectName = TestScriptBase.GetCurrentClassName();
            //this.TimeoutSec = Config.Settings.RunTimeSetting.ElementTimeoutSec;
        }

        public Element(ReadOnlyCollection<IWebElement> elements)
        {
            this.GetName();
            var eles = new List<Element>();
            foreach (var ele in elements)
            {
                eles.Add(new Element(ele));
            }

            //this.Elements_ = eles;
        }

        /// <summary>
        ///     Construct an element using an existing element
        /// </summary>
        /// <param name="element"></param>
        public Element(IWebElement element)
        {
            this.GetName();
            this.Element_ = element;

            //this.PageObjectName = TestScriptBase.GetCurrentClassName();
            //this.TimeoutSec = Config.Settings.RunTimeSetting.ElementTimeoutSec;
        }

        /// <summary>
        ///     Construct an element using an existing element
        /// </summary>
        /// <param name="element"></param>
        /// <param name="by"></param>
        public Element(IWebElement element, By by)
        {
            this.GetName();
            this.Element_ = element;
            this.By = by;
            //this.PageObjectName = TestBase.GetCurrentClassName();
            //this.TimeoutSec = Config.Settings.RunTimeSetting.ElementTimeoutSec;
        }

        /// <summary>
        ///     Construct an element
        /// </summary>
        /// <param name="name">Human readable name of the element</param>
        /// <param name="locator">By locator</param>
        public Element(string name, By locator)
        {
            this.GetName();
            this.Name = name;
            this.By = locator;
            //this.PageObjectName = TestBase.GetCurrentClassName();
            //this.TimeoutSec = Config.Settings.RunTimeSetting.ElementTimeoutSec;
        }

        /// <summary>
        ///     Construct an element
        /// </summary>
        /// <param name="locator">By locator</param>
        public Element(By locator)
        {
            this.GetName();
            this.By = locator;
            //this.PageObjectName = TestBase.GetCurrentClassName();
            //this.TimeoutSec = Config.Settings.RunTimeSetting.ElementTimeoutSec;
        }

        /// <summary>
        ///     Construct an element
        /// </summary>
        /// <param name="name">Human readable name of the element</param>
        /// <param name="locator">By locator</param>
        /// <param name="frame"></param>
        public Element(string name, By locator, Frame frame)
        {
            this.Frame1 = frame;
            this.Name = name;
            this.By = locator;
            //this.PageObjectName = TestBase.GetCurrentClassName();
            //this.TimeoutSec = Config.Settings.RunTimeSetting.ElementTimeoutSec;
        }

        /// <summary>
        ///     Construct an element
        /// </summary>
        /// <param name="locator">By locator</param>
        /// <param name="frame"></param>
        public Element(By locator, Frame frame)
        {
            this.GetName();
            this.Frame1 = frame;
            this.By = locator;
            //this.PageObjectName = TestBase.GetCurrentClassName();
            //this.TimeoutSec = Config.Settings.RunTimeSetting.ElementTimeoutSec;
        }

      

        /// <summary>
        ///     Checks if the element present on the page, but not necesarily displayed and visible
        /// </summary>
        public bool Present
        {
            get
            {
                try
                {
                    return this.Element_.Enabled;
                }
                catch (NoSuchElementException e)
                {
                    Console.WriteLine("Exception caught: No such element found\n" + e);
                    return false;
                }
                catch (StaleElementReferenceException e)
                {
                    Console.WriteLine("Exception caught: The Element Referenced is Stale\n" + e);
                    return false;
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine("Exception caught: This Operation is Invalid\n" + e);
                    return false;
                }
            }
        }

        /// <summary>
        ///     Checks if the element present and displayed on the page
        /// </summary>
        public bool Displayed
        {
            get
            {
                try
                {
                    if (!this.Present)
                    {
                        return false;
                    }

                    return this.Element_.Displayed;
                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                catch (StaleElementReferenceException e)
                {
                    return false;
                }
            }
        }

        /// <summary>
        ///     Checks if the element present and displayed on the page
        /// </summary>
        public bool Enabled
        {
            get
            {
                if (!this.Present)
                {
                    return false;
                }

                return this.Element_.Enabled;
            }
        }

        /// <summary>
        ///     Get the upper-left (x,y) coordinates of the element relative to the upper-left corner of the page.
        /// </summary>
        public Point Location
        {
            get { return this.Element_.Location; }
        }

        public Frame Frame
        {
            get
            {
                return this.Frame1;
            }

            set
            {
                this.Frame1 = value;
            }
        }

        /// <summary>
        ///     Checks if the element is selected on the page.
        /// </summary>
        public bool Selected
        {
            get
            {
                if (!this.Present)
                {
                    return false;
                }

                {
                    return this.Element_.Selected;
                }
            }
        }

        /// <summary>
        ///     Return an object containing the size of the element (height, width).
        /// </summary>
        public Size Size
        {
            get { return this.Element_.Size; }
        }

        /// <summary>
        ///     Return the tag name of the element.
        /// </summary>
        public string TagName
        {
            get { return this.Element_.TagName; }
        }

        /// <summary>
        ///     Property to get and set the Text for the element.
        /// </summary>
        public string Text
        {
            get
            {
                var text = this.Element_.Text;
                return text;
            }

            set
            {
                this.Element_.Clear();
                this.Element_.SendKeys(value);
            }
        }

        public IWebDriver WrappedDriver
        {
            get { return this.Driver; }
            private set { this.Driver = value; }
        }

        public IWebElement WrappedElement
        {
            get { return this.Element_; }
            private set { this.Element_ = value; }
        }

        public string Name
        {
            get
            {
                return this.Name1;
            }

            set
            {
                this.Name1 = value;
            }
        }

        public string Name1
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        public int TimeoutSec
        {
            get
            {
                return this.timeoutSec;
            }

            set
            {
                this.timeoutSec = value;
            }
        }

        public string PageObjectName
        {
            get
            {
                return this.pageObjectName;
            }

            set
            {
                this.pageObjectName = value;
            }
        }

        public By By
        {
            get
            {
                return this.by;
            }

            set
            {
                this.by = value;
            }
        }

        protected internal Frame Frame1
        {
            get
            {
                return this.frame1;
            }

            set
            {
                this.frame1 = value;
            }
        }

        protected internal Element Root
        {
            get
            {
                return this.root;
            }

            set
            {
                this.root = value;
            }
        }

        

       
        protected IWebElement Element_
        {
            get
            {
                //this.Element1 = this.GetElement();
                return this.Element1;
            }

            set
            {
                this.Element1 = value;
            }
        }

        

        protected IEnumerable<Element> Elements
        {
            get
            {
                return this.elements1;
            }

            set
            {
                this.elements1 = value;
            }
        }

        protected IWebElement Element1
        {
            get
            {
                return this.element;
            }

            set
            {
                this.element = value;
            }
        }

        public void GetName()
        {
            /*this.PageObjectName = TestBase.GetCurrentClassName();
            var stackTrace = new StackTrace(); // get call stack
            this.stackFrames = stackTrace.GetFrames(); // get method calls (frames)
            var t = this.GetType();

            foreach (var stackFrame in this.stackFrames)
            {
                var method = stackFrame.GetMethod();
                Type type = method.ReflectedType;
                if (method.Name.Contains("get_"))
                {
                    this.Name = $"{ this.PageObjectName}." + method.Name.Replace("get_", string.Empty).Replace("()", string.Empty);
                    if (this.Name.Contains("ClickOrdersLink"))
                    {
                        foreach (var stack in this.stackFrames)
                        {
                            Log.Message(stack.GetMethod().Name);
                        }
                    }

                    return;
                }

                if (type.IsSubclassOf(typeof(Element)) && (!method.IsConstructor))
                {
                    this.Name = $"{ this.PageObjectName}." + method.Name;
                    if (this.Name.Contains("ClickOrdersLick"))
                    {
                        foreach (var stack in this.stackFrames)
                        {
                            Log.Message(stack.GetMethod().Name);
                        }
                    }

                    return;
                }

                if (type.IsSubclassOf(typeof(BaseComponent)))
                {
                    this.Name = $"{ this.PageObjectName}." + type.Name;
                    if (this.Name.Contains("ClickOrdersLink"))
                    {
                        foreach (var stack in this.stackFrames)
                        {
                            Log.Message(stack.GetMethod().Name);
                        }
                    }

                    return;
                }
            }*/
        }

        public void ForEach(Action<Element> action)
        {
            this.ToList().ForEach(action);
        }

      

      

        /// <summary>
        /// Clears the contents of the element.
        /// </summary>
        /// <returns>The element reference</returns>
        public Element Clear()
        {
            try
            {
                this.Element_.Clear();
                return this;
            }
            catch (Exception e)
            {
                //Log.Warning("Could not clear " + e.Message);
                return this;
            }
        }



        /// <summary>
        ///     Submit this element to the web server and optionally highlights the element if set in the application configuration
        ///     settings.
        /// </summary>
        /// <returns>The element reference</returns>
        public Element Submit()
        {
            this.Element_.Submit();
            return this;
        }

        /// <summary>
        ///     Simulates typing text into the element and optionally highlights the element if set in the application
        ///     configuration settings.
        /// </summary>
        /// <param name="text">Text to send</param>
        public Element SendKeys(string text)
        {
            try
            {
                this.Element_.SendKeys(text);
            }
            catch (InvalidOperationException e)
            {
                throw new InvalidOperationException("Cannot sendKeys that element, please verify it is an input or text field" + e.Message);
            }

            return this;
        }

        /// <summary>
        ///     Get the value of the requested attribute for the element
        /// </summary>
        /// <param name="attribute">The attribute name</param>
        /// <returns></returns>
        public string GetAttribute(string attribute)
        {
            try
            {
                return this.Element_.GetAttribute(attribute);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        ///     Get the value of a CSS property for the element
        /// </summary>
        /// <param name="propertyName">The CSS property name</param>
        /// <returns></returns>
        public string GetCssValue(string propertyName)
        {
            return this.Element_.GetCssValue(propertyName);
        }

        /// <summary>
        ///     Click the element and optionally highlights the element if set in the application configuration settings.
        /// </summary>
        /// <returns>The element reference</returns>
        public Element Click()
        {
            try
            {
                this.Element_.Click();
            }
            catch (InvalidOperationException e)
            {
                this.Element_.Click();
                //Log.Message("Exception caught: This Operation is Invalid\n" + e);
            }

            return this;
        }



















        /// <summary>
        ///     Set the checkbox element
        /// </summary>
        /// <param name="isChecked">if true, check it; if false, uncheck it</param>
        /// <returns>The element reference</returns>
        public Element SetCheckbox(bool isChecked)
        {
            if (this.Element_.Selected != isChecked)
            {
                this.Element_.Click();
            }

            return this;
        }

       

        /// <summary>
        ///     Sends the Text for element
        /// </summary>
        /// <param name="value"></param>
        /// <returns>The element reference</returns>
        public Element SetText(string value)
        {
            this.Clear();
            this.SendKeys(value);
            var element_value = this.GetAttribute("value");
            if (value != element_value)
            {
                this.Clear();
                Thread.Sleep(1000);
                this.SendKeys(value);
            }
            return this;
        }

        

        

        /// <summary>
        ///     WithParam swaps out {0} in the locator with the value entered
        ///     This allows us to adjust for params with specific strings
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public Element WithParam(string value)
        {
            if (this.By == null)
            {
                throw new Exception("WithParams only works with Elements instantiated using a By locator");
            }

            var oldBy = this.By.ToString();
            var toks = oldBy.Split(':');
            var type = toks[0];
            var locator = toks[1];
            var newlocator = locator.Replace("{0}", value);
            if (type.Contains("ClassName"))
            {
                this.By = By.ClassName(newlocator);
            }

            if (type.Contains("XPath"))
            {
                this.By = By.XPath(newlocator);
            }

            if (type.Contains("Id"))
            {
                this.By = By.Id(newlocator);
            }

            if (type.Contains("PartialLink"))
            {
                this.By = By.PartialLinkText(newlocator);
            }

            if (type.Contains("LinkText"))
            {
                this.By = By.LinkText(newlocator);
            }

            if (type.Contains("Name"))
            {
                this.By = By.Name(newlocator);
            }

            if (type.Contains("CssSelector"))
            {
                this.By = By.CssSelector(newlocator);
            }

            if (type.Contains("TagName"))
            {
                this.By = By.TagName(newlocator);
            }

            return this;
        }

        /// <summary>
        ///     Swaps out {0},{1},{2}..etc with the values in values array
        ///     //div[contains(text(),'{0}) and contains(@class,'{1})]
        ///     with values[] = {"textOfElement","classofElement"} becomes
        ///     //div[contains(text(),'textOfElement') and contains(@class,'classOfElement)]
        ///     will not work if element was instantiated with an existing IWebELement instead of a By locator.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public Element WithParams(string[] values)
        {
            if (this.By == null)
            {
                throw new Exception("WithParams only works with Elements instantiated using a By locator");
            }

            var oldBy = this.By.ToString();
            var toks = oldBy.Split(':');
            var type = toks[0];
            var locator = toks[1];
            for (var i = 0; i < values.Length; i++)
            {
                locator = locator.Replace("{" + i + "}", values[i]);
            }

            if (type.Contains("ClassName"))
            {
                this.By = By.ClassName(locator);
            }

            if (type.Contains("XPath"))
            {
                this.By = By.XPath(locator);
            }

            if (type.Contains("Id"))
            {
                this.By = By.Id(locator);
            }

            if (type.Contains("PartialLink"))
            {
                this.By = By.PartialLinkText(locator);
            }

            if (type.Contains("LinkText"))
            {
                this.By = By.LinkText(locator);
            }

            if (type.Contains("Name"))
            {
                this.By = By.Name(locator);
            }

            if (type.Contains("CssSelector"))
            {
                this.By = By.CssSelector(locator);
            }

            if (type.Contains("TagName"))
            {
                this.By = By.TagName(locator);
            }

            return this;
        }

        private string GetFrameMessage()
        {
            if (this.Frame1 != null)
            {
                return " In frame " + this.Frame1.Name;
            }

            return string.Empty;
        }

        public IEnumerator<Element> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
