﻿using OpenQA.Selenium;
using SeleniumWithCSharp.NET.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.Extensions
{
    public class PageVerification : BasePage
    {
        /// <summary>
        /// This method takes a string list of possible page errors and verifies
        /// none of the listed errors are seen on the web page
        /// </summary>
        /// <param name="stringList">Lit of page Errors</param>
        public void VerifyPageIntegrity(List<string> stringList)
        {
            if (stringList.Any(x => Driver.PageSource.Contains(x)))
            {
                throw new WebDriverException(" ERROR : -->> URL " + '\"' + Driver.Url + '\"' +
                                             " DID NOT LOAD SMOOTHLY due to ERROR");
            }
        }

        /// <summary>
        /// This method returns false value of it finds any page errors like ,
        /// rendering exceptions, page not found etc. error on the page.
        /// </summary>
        /// <returns></returns>
        public bool CheckPageIntegrity(List<string> stringList)
        {
            if (stringList.Any(x => Driver.PageSource.Contains(x)))
            {
                //Log.Error($" ERROR :-->> URL " + '\"' + Driver.Url + '\"' +
                                             //" DID NOT LOAD SMOOTHLY due to ERROR");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
