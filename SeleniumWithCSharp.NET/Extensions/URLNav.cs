﻿using SeleniumWithCSharp.NET.Base;
using SeleniumWithCSharp.NET.PageObject;
using SeleniumWithCSharp.NET.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.Extensions
{
    public class URLNav : AutomationBaseHelpers
    {
        //private IConfig _configuration;
        private Config _configuration;
        private string currentUrl;
        //private AutomationBaseHelpers automationBase;
        private HomePageHelper homePageHelper;

        public URLNav()
        {
            _configuration = new Config();
            homePageHelper = new HomePageHelper();
            _configuration = new Config();
        }

        public string CurrentURL => currentUrl;

        public void GoToAutomationPractice(Browser.ScreenType size, string extension = "")
        {
            //Browser.ScreenType((int)size);

            // This line will grab url based on current configuration( QA,PROD etc)
            //Driver.Navigate().GoToUrl(_configuration.GetBaseUrl() + extension);
            currentUrl = Driver.Url;
            //WaitForQueueItRemoval(currentUrl);
            //VerifyNoPageErrors();
            //home.CurrencyDiscrepancyPopUp();
            //QuarantineMaritzCXSurvey();
        }
    }
}
