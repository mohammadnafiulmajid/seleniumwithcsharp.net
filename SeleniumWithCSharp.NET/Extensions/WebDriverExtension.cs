﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.Extensions
{
    public class WebDriverExtension : WebElementExtension
    {
        public void VerifyPageText(IWebElement element, string expectedText)
        {
            String actualText = element.Text;
            Assert.AreEqual(expectedText.Trim(), actualText.Trim(),"Actual and Expected results are not same");
        }

        public void WaitElapsedTime(uint mSec)
        {
            Boolean isReady = false;
            Stopwatch s = new Stopwatch();
            s.Start();
            while (!isReady)
            {
                if (s.ElapsedMilliseconds > mSec)
                    isReady = true;
            }

            s.Stop();
        }


    }
}
