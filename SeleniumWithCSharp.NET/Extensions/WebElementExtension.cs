﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumWithCSharp.NET.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.Extensions
{
    public class WebElementExtension
    {
        protected IWebDriver Driver = DriverFactory.GetInstance().GetDriver();

        public IWebElement WaitUntilElementExists(By elementLocator, int timeout = 10, bool ignorExceptions = false)
        {
            try
            {
                var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeout));

                if (ignorExceptions)
                    wait.IgnoreExceptionTypes(typeof(NoSuchElementException), typeof(ElementNotVisibleException));
                return wait.Until(ExpectedConditions.ElementExists(elementLocator));
            }catch(Exception e)
            {
                if (!ignorExceptions)
                    throw e;
                return null;
            }
        }
    }
}
