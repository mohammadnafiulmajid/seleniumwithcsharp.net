﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.Base
{
    public class DriverFactory
    {
        private static DriverFactory instance = null;

        private DriverFactory()
        {
        }

        public static DriverFactory GetInstance()
        {
            if(instance == null)
            {
                instance = new DriverFactory();
            }
            return instance;
        }

        public static DriverFactory GetInstance(String browserName)
        {
            var driverDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (instance == null)
            {
                instance = new DriverFactory();
            }
            if (browserName.Equals("Chrome"))
            {
                instance.driver.Value = new ChromeDriver(driverDir);
            }
            if (browserName.Equals("FireFox"))
            {
                instance.driver.Value = new FirefoxDriver(driverDir);
            }
            return instance;
        }

        ThreadLocal<IWebDriver> driver = BuildThreadLocal();

        private static ThreadLocal<IWebDriver> BuildThreadLocal()
        {
            return new ThreadLocal<IWebDriver>(() =>
            {
                return new ChromeDriver();
            });
        }

        public IWebDriver GetDriver()
        {
            return driver.Value;
        }

        public void RemoveDriver()
        {
            driver.Value.Dispose();
            driver.Value.Close();
            driver.Value.Quit();
            driver.Values.Clear();
        }
    }
}
