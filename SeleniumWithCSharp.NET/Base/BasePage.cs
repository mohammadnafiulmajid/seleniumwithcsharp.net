﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumWithCSharp.NET.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UAParser;

namespace SeleniumWithCSharp.NET.Base
{
   public class BasePage : WebDriverExtension
    {

        public void VerifyPageTitle(string expectedTitle)
        {
            String title = Driver.Title;
            Assert.AreEqual(expectedTitle,title);
        }

        protected void Highlight(IWebElement element)
        {
            for (int i = 0; i < 2; i++)
            {
                IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
                js.ExecuteScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 2px solid red;");
                Thread.Sleep(300);
                js.ExecuteScript(
                        "arguments[0].setAttribute('style', arguments[1]);",
                        element, "");
                Thread.Sleep(300);
            }
        }        

        public void TypeText(IWebElement element, string text)
        {
            if (element == null) throw new NullReferenceException();
            try
            {
                Highlight(element);
                element.Clear();
                element.SendKeys(text);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void Click(IWebElement element)
        {
            if (element == null) throw new NullReferenceException();
            try
            {
                element.Click();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public bool IsPageReady()
        {
            try
            {
                IWait<IWebDriver> wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(30.00));
                wait.Until(Driver => ((IJavaScriptExecutor)Driver).ExecuteScript("return document.readyState").Equals("complete"));
            }
            catch (WebDriverException e)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns the Browser run type (i.e. Chrome, IE, FireFox)
        /// </summary>
        /// <returns></returns>
        public string GetBrowserType()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            string uaString = (string)(js.ExecuteScript("return navigator.userAgent"));
            var uaParser = Parser.GetDefault();
            ClientInfo c = uaParser.Parse(uaString);

            return c.UserAgent.Family;
        }

        /// <summary>
        /// Methods adds provided time out for the IE browser
        /// </summary>
        /// <param name="sleeptime"></param>
        public void LapsedTimeIfIEBrowser(uint sleepTime)
        {

            string userAgent = GetBrowserType();

            System.Console.WriteLine("Env: " + userAgent);
            if (userAgent.Contains("IE"))
            {
                WaitElapsedTime(sleepTime);
            }
        }

        /// <summary>
        /// Methods adds provided time out for the Firefox browser
        /// </summary>
        /// <param name="sleeptime"></param>
        public void LapsedTimeIFirefoxEBrowser(uint sleepTime)
        {

            string userAgent = GetBrowserType();

            System.Console.WriteLine("Env: " + userAgent);
            if (userAgent.Contains("Firefox"))
            {
                WaitElapsedTime(sleepTime);
            }
        }        

        /// <summary>
        /// Methods adds provided time out for the IE browser
        /// </summary>
        /// <param name="sleeptime"></param>
        public void ClickMultipleTimesIfIEorFireFoxBrowser(IWebElement element, IWebElement expectedElement, int n)
        {
            string userAgent = GetBrowserType();

            if (expectedElement.Displayed)
            {
                return;
            }

            Console.WriteLine("Env: " + userAgent);
            if (userAgent.Contains("IE") || userAgent.Contains("Firefox"))
            {
                for (int i = 0; i < n; i++)
                {
                    element.Click();
                    WaitElapsedTime(2000);
                    if (expectedElement.Displayed)
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// This method takes the Y location value for given element and moves up by give offsetY
        /// </summary>
        /// <param name="element">Elements whose Y value is taken as reference for scrolling</param>
        /// <param name="offsetY">Scroll up by this Y offset </param>
        public void ScrollUpByGivenYCoordinate(Element element, int offsetY)
        {
            var loc = element.Location;
            ((IJavaScriptExecutor)Driver).ExecuteScript($@"scroll(0,'{loc.Y - offsetY}');");
        }

        /// <summary>
        /// Select one of many Elements on the page that have the same identifier, based on text.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="text"></param>
        public void SelectElementWithText(Element element, string text)
        {
            int i = 0;
            int elementCount = element.Count();

            while (i < elementCount)
            {
                if (element.Skip(i).First().Text.Contains(text) || element.Skip(i).First().GetAttribute("innerText").Contains(text))
                {
                    element.Click(); break;
                }
                else
                {
                    i++;
                }
            }
        }

        /// <summary>
        /// Generates random strings with a given length
        /// </summary>
        /// <param name="size">Size of the string</param>
        /// <param name="lowerCase">If true, generate lowercase string</param>
        /// <returns>Random string</returns>
        public string RandomString(int size = 4, bool lowerCase = true)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 1; i < size + 1; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            else
                return builder.ToString();
        }
    }
    }

