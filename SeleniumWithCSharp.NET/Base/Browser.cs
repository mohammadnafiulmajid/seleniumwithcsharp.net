﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumWithCSharp.NET.Base
{
    public class Browser : BasePage
    {
        public BrowerType Type { get; set; }

        public enum BrowerType
        {
            InternetExplorer,
            Firefox,
            Chrome,
            Edge
        }

        public enum ScreenType
        {
            Laptop,
            SmartPhone,
            TabletPortrait,
            TabletLandScape,
            BigScreen
        }

        public void OpenScreen(ScreenType screenType)
        {
            switch (screenType)
            {
                case ScreenType.Laptop:
                    Driver.Manage().Window.Size= new Size(1919, 1024);
                    break;
                case ScreenType.SmartPhone:
                    Driver.Manage().Window.Size = new Size(320, 767);
                    break;
                case ScreenType.TabletPortrait:
                    Driver.Manage().Window.Size = new Size(768, 1023);
                    break;
                case ScreenType.TabletLandScape:
                    Driver.Manage().Window.Size = new Size(1920, 1080);
                    break;
                case ScreenType.BigScreen:
                    Driver.Manage().Window.Size = new Size(1024, 1279);
                    break;
            }
        }
    }
}
