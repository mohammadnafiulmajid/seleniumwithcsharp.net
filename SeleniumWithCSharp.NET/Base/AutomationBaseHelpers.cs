﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using SeleniumWithCSharp.NET.Base;
using SeleniumWithCSharp.NET.PageObject.Login;
using SeleniumWithCSharp.NET.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SeleniumWithCSharp.NET.Extensions
{
    public class AutomationBaseHelpers : BasePage
    {
        private IConfig configuration;       
        PageVerification pageVerification = new PageVerification();
        public JsonUserInfoHelpers jsonUserInfoHelpers = new JsonUserInfoHelpers();

        public AutomationBaseHelpers()
        {
            configuration = new Settings.Config();
        }

        //Read Pdf file
        public static string ExtractTextFromPDF(string pdfFileName)
        {
            StringBuilder result = new StringBuilder();
            // Create a reader for the given PDF file
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | (SecurityProtocolType)3072;
            using (PdfReader reader = new PdfReader(pdfFileName))
            {
                // Read pages
                for (int page = 1; page <= reader.NumberOfPages; page++)
                {
                    SimpleTextExtractionStrategy strategy =
                        new SimpleTextExtractionStrategy();
                    string pageText =
                        PdfTextExtractor.GetTextFromPage(reader, page, strategy);
                    result.Append(pageText);
                }
            }
            return result.ToString();
        }

        public string GetValueFromXmlFile(string filepath, string xpath)
        {
            var configFile = new XmlDocument();
            configFile.Load(filepath);
            return configFile.SelectSingleNode(xpath).Value ?? string.Empty;
        }
    }
}
